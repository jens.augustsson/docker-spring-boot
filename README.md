Hello World sample shows how to deploy [SpringBoot](http://projects.spring.io/spring-boot/) [RESTful web service](https://spring.io/understanding/REST) application with [Docker](https://www.docker.com/)
This code is initially cloned from https://github.com/dstar55/docker-hello-world-spring-boot

Note that any fat jar in target folder will be run by Docker, so SpringBoot may be replaced by e.g Thorntail

## Prerequisite

Installed: Docker, Java 1.8, Maven 3.x

## Clone source code from git
```
$  git clone https://gitlab.com/jens.augustsson/docker-spring-boot .
```

## Build and run
```
mvn clean install && docker run -it --rm -p 80:8080 $(docker build -q .)
```

## Test
```
$ curl localhost
```

the response should be
```
Hello World
```
