#pull base image
FROM openjdk:8-jdk-alpine

#maintainer 
MAINTAINER jens@augustsson.eu

#copy hello world to docker image
COPY ./target/*.jar /data/

#expose port 8080
EXPOSE 8080

#default command
CMD java -jar /data/*.jar
